-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Хост: localhost:3306
-- Время создания: Дек 07 2020 г., 15:12
-- Версия сервера: 8.0.22-0ubuntu0.20.04.2
-- Версия PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `60971shai`
--

-- --------------------------------------------------------

--
-- Структура таблицы `Бронирование`
--

CREATE TABLE `Бронирование` (
  `id` int NOT NULL COMMENT 'ID бронирования',
  `ID_ROOM` int NOT NULL COMMENT 'ID комнаты',
  `ID_PEOPLE` int NOT NULL COMMENT 'ID гостя',
  `DateStart` date NOT NULL COMMENT 'Начало проживания',
  `DateEnd` date NOT NULL COMMENT 'Конец проживания',
  `NumPeople` int NOT NULL COMMENT 'Кол-во человек'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `Бронирование`
--

INSERT INTO `Бронирование` (`id`, `ID_ROOM`, `ID_PEOPLE`, `DateStart`, `DateEnd`, `NumPeople`) VALUES
(1, 1, 1, '2020-12-01', '2020-12-07', 4),
(2, 2, 24, '2020-12-10', '2020-12-24', 1),
(3, 5, 9, '2020-11-25', '2020-11-27', 1),
(4, 7, 22, '2020-12-30', '2021-01-08', 4),
(5, 12, 6, '2020-12-31', '2021-01-03', 7),
(6, 20, 18, '2020-11-01', '2020-11-04', 3),
(7, 17, 12, '2020-10-01', '2020-10-14', 1),
(8, 26, 13, '2020-12-11', '2020-12-19', 4),
(9, 35, 2, '2020-12-14', '2020-12-28', 4),
(10, 39, 23, '2020-10-09', '2020-10-30', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `Гость`
--

CREATE TABLE `Гость` (
  `id` int NOT NULL COMMENT 'ID гостя',
  `FullName` varchar(255) NOT NULL COMMENT 'ФИО'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `Гость`
--

INSERT INTO `Гость` (`id`, `FullName`) VALUES
(1, 'Шевчук А.И.'),
(2, 'Баженова А.С.'),
(3, 'Кузнецов А.А.'),
(4, 'Потапов В.А.'),
(5, 'Корепанов А.А.'),
(6, 'Иванов В.В.'),
(7, 'Пупкин Ф.К.'),
(8, 'Краснопёров С.В.'),
(9, 'Алексеев М.Е.'),
(10, 'Архарова Т.И.'),
(11, 'Голубев Д.О.'),
(12, 'Марьина З.Х.'),
(13, 'Семёнов А.И.'),
(14, 'Большаков Г.М.'),
(15, 'Тихонов Я.В.'),
(16, 'Данилов В.В.'),
(17, 'Харитонова И.Р.'),
(18, 'Киршин П.С.'),
(19, 'Матюшина М.П.'),
(20, 'Петухов А.М.'),
(21, 'Заверзин К.Е.'),
(22, 'Кадышевв Д.В'),
(23, 'Петрухина Е.И.'),
(24, 'Аникин П.Р'),
(25, 'Гвоздева Р.В'),
(26, 'Гришин Р.П.'),
(27, 'Каталов К.Т.');

-- --------------------------------------------------------

--
-- Структура таблицы `Комната`
--

CREATE TABLE `Комната` (
  `id` int NOT NULL COMMENT 'ID комнаты',
  `ID_HOUSING` int NOT NULL COMMENT 'ID корпуса',
  `NumRoom` int NOT NULL COMMENT 'Номер комнаты',
  `NumOfSeats` int NOT NULL COMMENT 'Кол-во мест',
  `Cost` int NOT NULL COMMENT 'Цена'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `Комната`
--

INSERT INTO `Комната` (`id`, `ID_HOUSING`, `NumRoom`, `NumOfSeats`, `Cost`) VALUES
(1, 1, 1, 4, 9500),
(2, 1, 2, 1, 1550),
(3, 1, 3, 2, 2400),
(4, 1, 4, 2, 3000),
(5, 1, 5, 1, 3000),
(6, 1, 6, 1, 1800),
(7, 1, 7, 4, 10440),
(8, 1, 8, 3, 6750),
(9, 2, 1, 5, 11400),
(10, 2, 2, 4, 9900),
(11, 2, 3, 1, 1490),
(12, 2, 4, 7, 15900),
(13, 2, 5, 4, 9000),
(14, 2, 6, 2, 6000),
(15, 2, 7, 2, 4500),
(16, 3, 1, 1, 550),
(17, 3, 2, 1, 700),
(18, 3, 3, 2, 700),
(19, 3, 4, 2, 950),
(20, 3, 5, 3, 1000),
(21, 3, 6, 1, 1050),
(22, 4, 10, 2, 10000),
(23, 4, 11, 1, 7000),
(24, 4, 12, 3, 15000),
(25, 4, 13, 2, 11500),
(26, 4, 14, 4, 15000),
(27, 4, 15, 4, 12300),
(28, 4, 16, 3, 9000),
(29, 4, 17, 4, 12300),
(30, 4, 18, 1, 7890),
(31, 5, 31, 5, 35000),
(32, 5, 33, 3, 20000),
(33, 5, 36, 1, 9000),
(34, 5, 37, 1, 15770),
(35, 5, 41, 4, 32000),
(36, 5, 42, 1, 10220),
(37, 5, 52, 2, 21300),
(38, 5, 53, 3, 25900),
(39, 6, 1, 1, 450),
(40, 6, 2, 1, 250),
(41, 6, 3, 2, 700),
(42, 6, 4, 3, 550),
(43, 6, 5, 1, 600),
(44, 6, 6, 2, 400),
(45, 7, 256, 4, 3500),
(46, 7, 258, 2, 2550),
(47, 7, 259, 1, 2460),
(48, 7, 260, 1, 3000),
(49, 8, 20, 7, 10200),
(50, 8, 21, 5, 8000),
(51, 8, 22, 10, 14750),
(52, 8, 23, 8, 12900);

-- --------------------------------------------------------

--
-- Структура таблицы `Корпус`
--

CREATE TABLE `Корпус` (
  `id` int NOT NULL COMMENT 'ID корпуса',
  `name_house` varchar(255) NOT NULL COMMENT 'Наименование'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `Корпус`
--

INSERT INTO `Корпус` (`id`, `name_house`) VALUES
(1, 'Снегирь'),
(2, 'Сосновый бор'),
(3, 'Тетрис Холл'),
(4, 'Даниловская мануфактура'),
(5, 'Эталон'),
(6, 'Георгиевский'),
(7, 'Возрождение'),
(8, 'За Ручьём');

-- --------------------------------------------------------

--
-- Структура таблицы `Проживание`
--

CREATE TABLE `Проживание` (
  `id` int NOT NULL COMMENT 'ID проживания',
  `ID_ROOM` int NOT NULL COMMENT 'ID комнаты',
  `ID_PEOPLE` int NOT NULL COMMENT 'ID гостя',
  `DateStart` date NOT NULL COMMENT 'Начало проживания',
  `DateEnd` date NOT NULL COMMENT 'Конец проживания',
  `NumPeople` int NOT NULL COMMENT 'Кол-во человек'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `Проживание`
--

INSERT INTO `Проживание` (`id`, `ID_ROOM`, `ID_PEOPLE`, `DateStart`, `DateEnd`, `NumPeople`) VALUES
(1, 1, 1, '2020-12-01', '2020-12-07', 4),
(2, 2, 24, '2020-12-10', '2020-12-24', 1),
(3, 5, 9, '2020-11-25', '2020-12-27', 1),
(4, 26, 13, '2020-12-11', '2020-12-19', 4),
(5, 35, 2, '2020-12-14', '2020-12-28', 4);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `Бронирование`
--
ALTER TABLE `Бронирование`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ID_ROOM` (`ID_ROOM`),
  ADD KEY `ID_PEOPLE` (`ID_PEOPLE`);

--
-- Индексы таблицы `Гость`
--
ALTER TABLE `Гость`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `Комната`
--
ALTER TABLE `Комната`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ID_HOUSING` (`ID_HOUSING`);

--
-- Индексы таблицы `Корпус`
--
ALTER TABLE `Корпус`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `Проживание`
--
ALTER TABLE `Проживание`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ID_ROOM` (`ID_ROOM`),
  ADD KEY `ID_PEOPLE` (`ID_PEOPLE`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `Бронирование`
--
ALTER TABLE `Бронирование`
  MODIFY `id` int NOT NULL AUTO_INCREMENT COMMENT 'ID бронирования', AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT для таблицы `Гость`
--
ALTER TABLE `Гость`
  MODIFY `id` int NOT NULL AUTO_INCREMENT COMMENT 'ID гостя', AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT для таблицы `Комната`
--
ALTER TABLE `Комната`
  MODIFY `id` int NOT NULL AUTO_INCREMENT COMMENT 'ID комнаты', AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT для таблицы `Корпус`
--
ALTER TABLE `Корпус`
  MODIFY `id` int NOT NULL AUTO_INCREMENT COMMENT 'ID корпуса', AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT для таблицы `Проживание`
--
ALTER TABLE `Проживание`
  MODIFY `id` int NOT NULL AUTO_INCREMENT COMMENT 'ID проживания', AUTO_INCREMENT=6;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `Бронирование`
--
ALTER TABLE `Бронирование`
  ADD CONSTRAINT `Бронирование_ibfk_1` FOREIGN KEY (`ID_ROOM`) REFERENCES `Комната` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `Бронирование_ibfk_2` FOREIGN KEY (`ID_PEOPLE`) REFERENCES `Гость` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Ограничения внешнего ключа таблицы `Комната`
--
ALTER TABLE `Комната`
  ADD CONSTRAINT `Комната_ibfk_1` FOREIGN KEY (`ID_HOUSING`) REFERENCES `Корпус` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Ограничения внешнего ключа таблицы `Корпус`
--
ALTER TABLE `Корпус`
  ADD CONSTRAINT `Корпус_ibfk_1` FOREIGN KEY (`id`) REFERENCES `Комната` (`ID_HOUSING`);

--
-- Ограничения внешнего ключа таблицы `Проживание`
--
ALTER TABLE `Проживание`
  ADD CONSTRAINT `Проживание_ibfk_1` FOREIGN KEY (`ID_ROOM`) REFERENCES `Бронирование` (`ID_ROOM`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `Проживание_ibfk_2` FOREIGN KEY (`ID_PEOPLE`) REFERENCES `Бронирование` (`ID_PEOPLE`) ON DELETE RESTRICT ON UPDATE RESTRICT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
